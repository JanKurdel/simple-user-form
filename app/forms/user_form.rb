class UserForm
  # NOTE: consider extracting this to validation spefic class and use Dry::Types::Struct for form object
  Form = Dry::Validation.Form do
    configure do
      predicates(EmailPredicate)
      config.messages_file = Rails.root.join('config/locales/errors.yml')

      def unique?(value)
        !User.exists?(email: value)
      end
    end

    required(:name).filled(:str?)
    required(:email) { filled? & email? & unique? }

    required(:address).schema do
      required(:city).filled(:str?)
      required(:street).maybe(:str?)
      required(:street_number).maybe(:int?)
    end
  end
end
