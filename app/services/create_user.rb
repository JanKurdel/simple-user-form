class CreateUser
  def initialize(params)
    @params = params
  end

  def call
    form = UserForm::Form.call(@params)

    if form.errors.blank?
      create_user(form.output)
    else
      form
    end
  end

  private

  def create_user(attributes)
    address_attributes = attributes.delete(:address)
    User.transaction do
      user = User.create(attributes)
      user.create_address(address_attributes)
    end
  end
end
