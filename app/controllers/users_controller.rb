class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    service = CreateUser.new(params[:user])

    @user = service.call
    if @user.errors.blank?
      redirect_to users_path
    else
      render 'new'
    end
  end
end
