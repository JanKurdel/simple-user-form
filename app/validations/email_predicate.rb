module EmailPredicate
  include Dry::Logic::Predicates

  EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  predicate(:email?) do |value|
    !EMAIL_REGEX.match(value).nil?
  end
end
