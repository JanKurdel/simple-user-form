require "rails_helper"

RSpec.describe CreateUser do
  let(:user) { { email: 'fake@mail.com', name: 'John Smith' } }
  let(:address) { { city: 'Warsaw', street: 'Marszalkowska', street_number: 13 } }

  let(:email) { 'fake@mail.com' }
  let(:name) { 'John Smith' }
  let(:city) { 'Warsaw' }
  let(:street) { 'Marszalkowska' }
  let(:street_number) { 123 }

  let(:form_params) do
    {
      email: email,
      name: name,
      address: {
        city: city,
        street: street,
        street_number: street_number
      }
    }
  end

  context 'with correct user and addrees' do
    it 'should create user when all fields are filled in' do
      service = CreateUser.new(form_params)
      expect { service.call }.to change { User.count }.by(1)
    end

    it 'should create user when only required fields are filled in' do
      form_params[:address][:street] = nil
      form_params[:address][:street_number] = nil
      service = CreateUser.new(form_params)
      expect { service.call }.to change { User.count }.by(1)
    end
  end

  context 'with wrong address and correct user' do
    let(:wrong_street_number) { 'numbers should be numbers' }

    it 'should not create user without city filled in'  do
      form_params[:address][:city] = nil
      service = CreateUser.new(form_params)
      expect { service.call }.to_not change { User.count }
    end

    it 'should not create user with not integer street number'  do
      form_params[:address][:street_number] = wrong_street_number
      service = CreateUser.new(form_params)
      expect { service.call }.to_not change { User.count }
    end
  end

  context 'with wrong user and correct address' do
    let(:wrong_email) { 'it-is-not-a-valid@mail' }

    it 'should not create user with wrong formatted email address'  do
      form_params[:email] = wrong_email
      service = CreateUser.new(form_params)
      expect { service.call }.to_not change { User.count }
    end

    it 'should not create user without email filled in' do
      form_params[:email] = nil
      service = CreateUser.new(form_params)
      expect { service.call }.to_not change { User.count }
    end

    it 'should not create user without name filled in' do
      form_params[:name] = nil
      service = CreateUser.new(form_params)
      expect { service.call }.to_not change { User.count }
    end

    it 'should not create user with duplicated email address', fails: true do
      service = CreateUser.new(form_params)
      service.call
      expect { service.call }.to_not change { User.count }
    end
  end
end
