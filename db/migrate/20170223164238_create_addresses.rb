class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :city, null: false
      t.integer :street_number

      t.belongs_to :user, null: false

      t.timestamps null: false
    end
  end
end
